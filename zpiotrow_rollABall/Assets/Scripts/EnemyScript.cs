﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScript : MonoBehaviour
{
    //connect the script to player in unity
    public Transform Player;
    public float MoveSpeed = 0.2f;
    int MaxDist = 11;
    int MinDist = 0;



    void Start()
    {

    }
    //on update, follow the Player -designated in unity
    void Update()
    {
        transform.LookAt(Player);
        //if the distance is greater than the minimum distance, head towards player
        if (Vector3.Distance(transform.position, Player.position) >= MinDist)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;


            //the closer player gets to enemy, the faster the movement speed will be. 
            if (Vector3.Distance(transform.position, Player.position) <= MaxDist)
            {
                MoveSpeed += 0.005f;
            }

        }
    }// this code is from tutorial : https://answers.unity.com/questions/274809/how-to-make-enemy-chase-player-basic-ai.html
}