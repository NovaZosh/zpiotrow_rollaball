﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text CountText;
    public Text WinText;

    private Rigidbody rb;
    private int count;

    bool onGround =true; //Whether the player is on the ground, if they are they cn jump
    public float jumpPower; //how high player can jump


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        WinText.text = "";
    }

    // Update is called once per frame

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.other.CompareTag("Enemy"))//if the thing we ran into is enemy
        {
            RestartLevel();
        }
  
    }
    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        /*in the following if player presses space, the ball jumps
        The code is from this tutorial: https://www.youtube.com/watch?v=yvX4ttbWLOQ
        I wanted to be able to double jump to a certain height. 
         */
        if (Input.GetKeyDown(KeyCode.Space)) //press space
        {
            if (transform.position.y <= 2.05f) //if y is less than 2.05
            {
                GetComponent<Rigidbody>().AddForce(Vector3.up * 150); //add upward force to rigidbody
            }
        }
        //the below makes it so that if the ball falls from the stage, the level resets
        if (transform.position.y <-10f) {
            RestartLevel();
        }
         
    }
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        //if the ball runs into the pickup, the pickup count increases
        //the pickup also disappears.
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        //the below script runs if the coin is collided with.
        //it then sets the coin to inactive and sets walls variable to false
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
        }
    }
    
        //raycast is form of physics detection - shoots lazer out and if it hits something, it returns yes/no
        //we say the direction to shoot it, the direction, and how far

    void SetCountText()
    {
        CountText.text = "Count: " + count.ToString();
        if (count >= 13)
        {
            WinText.text = "You Win!";
            Invoke("RestartLevel", 2f);
        }
    }
    void RestartLevel()//this makes the level reset
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}


